﻿namespace TranslateTray.Core
{
    public interface ITranslationClient
    {
        string Translate(string input);
    }
}