# TranslateTray

Just something quick for now to help me out with translating things in Swedish as I go.

Initial working version put together in literally under 30 minutes.
I'm spending more time on publishing it to GitHub than I did on the acutal project.

Caveats:

* Windows only
* Hard-coded from Swedish to English

Would like to add:

* Mono-ize so can run on Linux (not sure how much work involved with UI but would be a good learning exercise)
* nicer UI
* detect language automatically
* only display if language matched
* detect selected text from any window without needing to copy to clipboard

Sharing because it might be useful. Probably never going to update because it's good enough for what I need right now.

Enjoy. Or don't.